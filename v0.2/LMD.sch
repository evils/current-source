EESchema Schematic File Version 2
LIBS:lm399
LIBS:sensors
LIBS:stm32
LIBS:references
LIBS:onsemi
LIBS:maxim
LIBS:ac-dc
LIBS:analog_devices
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:adc-dac
LIBS:microcontrollers
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:audio
LIBS:interface
LIBS:philips
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:transf
LIBS:rfcom
LIBS:current source v0,2-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Switch_SPDT_x2 SW?
U 1 1 5A24EDDA
P 4500 2550
F 0 "SW?" H 4300 2700 50  0000 C CNN
F 1 "Switch_SPDT_x2" H 4250 2400 50  0001 C CNN
F 2 "" H 4500 2550 50  0000 C CNN
F 3 "" H 4500 2550 50  0000 C CNN
	1    4500 2550
	0    1    1    0   
$EndComp
$Comp
L Switch_SPDT_x2 SW?
U 2 1 5A24EE08
P 4900 2550
F 0 "SW?" H 4700 2700 50  0000 C CNN
F 1 "Switch_SPDT_x2" H 4650 2400 50  0001 C CNN
F 2 "" H 4900 2550 50  0000 C CNN
F 3 "" H 4900 2550 50  0000 C CNN
	2    4900 2550
	0    1    1    0   
$EndComp
$Comp
L Switch_SPDT_x2 SW?
U 1 1 5A24EE7B
P 4000 2550
F 0 "SW?" H 3800 2400 50  0000 C CNN
F 1 "Switch_SPDT_x2" H 3750 2400 50  0001 C CNN
F 2 "" H 4000 2550 50  0000 C CNN
F 3 "" H 4000 2550 50  0000 C CNN
	1    4000 2550
	0    1    1    0   
$EndComp
$Comp
L Switch_SPDT_x2 SW?
U 2 1 5A24EEB9
P 4000 2550
F 0 "SW?" H 3800 2700 50  0000 C CNN
F 1 "Switch_SPDT_x2" H 3750 2400 50  0001 C CNN
F 2 "" H 4000 2550 50  0000 C CNN
F 3 "" H 4000 2550 50  0000 C CNN
	2    4000 2550
	0    1    1    0   
$EndComp
$Comp
L R_Small R?
U 1 1 5A24F4B6
P 3700 1900
F 0 "R?" V 3600 1850 50  0000 L CNN
F 1 "800R" V 3800 1800 50  0000 L CNN
F 2 "" H 3700 1900 50  0000 C CNN
F 3 "" H 3700 1900 50  0000 C CNN
	1    3700 1900
	0    1    1    0   
$EndComp
$Comp
L R_Small R?
U 1 1 5A24F626
P 4000 2050
F 0 "R?" H 3850 2000 50  0000 L CNN
F 1 "800R" H 3750 2100 50  0000 L CNN
F 2 "" H 4000 2050 50  0000 C CNN
F 3 "" H 4000 2050 50  0000 C CNN
	1    4000 2050
	-1   0    0    1   
$EndComp
$Comp
L R_Small R?
U 1 1 5A24F769
P 4500 2050
F 0 "R?" H 4350 2000 50  0000 L CNN
F 1 "200R" H 4250 2100 50  0000 L CNN
F 2 "" H 4500 2050 50  0000 C CNN
F 3 "" H 4500 2050 50  0000 C CNN
	1    4500 2050
	-1   0    0    1   
$EndComp
Wire Wire Line
	4500 2150 4500 2250
Wire Wire Line
	4000 2250 4000 2250
Wire Wire Line
	3900 2850 3900 2850
Wire Wire Line
	4100 2850 4100 2850
Wire Wire Line
	4900 2150 4900 2250
Wire Wire Line
	4000 2250 4000 2150
Wire Wire Line
	4000 2200 4250 2200
Wire Wire Line
	4250 2200 4250 2900
Wire Wire Line
	4250 2900 4400 2900
Wire Wire Line
	4400 2900 4400 2850
Connection ~ 4000 2200
Wire Wire Line
	3800 1900 5250 1900
Wire Wire Line
	4900 1900 4900 1950
Wire Wire Line
	4500 1950 4500 1900
Connection ~ 4500 1900
Wire Wire Line
	4000 1950 4000 1900
Connection ~ 4000 1900
Wire Wire Line
	3900 2850 3900 2950
Wire Wire Line
	3400 2950 4800 2950
Wire Wire Line
	4800 2950 4800 2850
Wire Notes Line
	3500 1700 5150 1700
Wire Notes Line
	5150 1500 5150 3050
Wire Notes Line
	5150 3050 3500 3050
Wire Notes Line
	4300 3050 4300 1700
Wire Notes Line
	3500 3050 3500 1500
Text Notes 3900 1850 0    60   ~ 0
1:2
Text Notes 4800 1850 0    60   ~ 0
1:5
Text Notes 4050 1650 0    60   ~ 0
1:1-2-5-10
Wire Notes Line
	3500 1500 5150 1500
Text HLabel 3400 1900 0    60   Input ~ 0
In
Text HLabel 5250 1900 2    60   Input ~ 0
Out
Text HLabel 3400 2950 0    60   Input ~ 0
GND
Connection ~ 3900 2950
Wire Wire Line
	3600 1900 3400 1900
Connection ~ 4900 1900
$Comp
L R_Small R?
U 1 1 5A26A91D
P 4900 2050
F 0 "R?" H 4750 2000 50  0000 L CNN
F 1 "200R" H 4650 2100 50  0000 L CNN
F 2 "" H 4900 2050 50  0000 C CNN
F 3 "" H 4900 2050 50  0000 C CNN
	1    4900 2050
	-1   0    0    1   
$EndComp
$EndSCHEMATC
